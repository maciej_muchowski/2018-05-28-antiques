package pl.codementors.antiques;

import pl.codementors.antiques.model.Antique;
import pl.codementors.antiques.model.Country;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.logging.Logger;

/**
 * Application store context.
 */
@Singleton
@Startup
public class AntiquesStoreContext {

    /**
     * Logger.
     */
    private static final Logger log = Logger.getLogger(AntiquesStoreContext.class.getCanonicalName());

    /**
     * Entity manager.
     */
    @PersistenceContext
    private EntityManager em;

    /**
     * Injected store.
     */
    @Inject
    private AntiquesStore store;

    /**
     * Method adding antiques and countries at post construct only if db doesn't contain any antique.
     */
    @PostConstruct
    public void init(){
        if(store.findAntiques().size() == 0){
            Country poland = new Country("Poland");
            em.persist(poland);

            Country japan = new Country("Japan");
            em.persist(japan);

            Country china = new Country("China");
            em.persist(china);

            Country france = new Country("France");
            em.persist(france);

            Country spain = new Country("Spain");
            em.persist(spain);

            Country portugal = new Country("Portugal");
            em.persist(portugal);

            Antique antique1 = new Antique(poland,1886,30000);
            antique1.setAvailable(true);
            em.persist(antique1);

            Antique antique2 = new Antique(japan,1820,60000);
            antique2.setAvailable(true);
            em.persist(antique2);

            Antique antique3 = new Antique(poland,1946,25000);
            antique3.setAvailable(true);
            em.persist(antique3);
        }
    }
}
