package pl.codementors.antiques.converters;

import pl.codementors.antiques.AntiquesStore;
import pl.codementors.antiques.model.Country;


import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Converter for Country class.
 */
@FacesConverter("countriesConverter")
public class CountryConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        AntiquesStore store = CDI.current().select(AntiquesStore.class).get();
        return store.getCountry(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "null";
        }
        return ((Country) value).getId() + "";
    }
}
