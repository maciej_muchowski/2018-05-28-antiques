package pl.codementors.antiques;


import pl.codementors.antiques.model.Antique;
import pl.codementors.antiques.model.Client;
import pl.codementors.antiques.model.Country;
import pl.codementors.antiques.model.Order;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Optional;

/**
 * Application's DAO.
 */
@Stateless
@Path("antiques")
public class AntiquesStore {

    /**
     * Entity manager.
     */
    @PersistenceContext
    private EntityManager em;

    private Client client;

    /**
     * Method getting all antiques from db.
     * @return List of antiques.
     */
    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Antique> findAntiques() {
        return em.createQuery("SELECT a FROM Antique a").getResultList();
    }

    /**
     * Method getting single antique from db. Or if antique do not exist, then returns new antique.
     * @param id Searched antique's id.
     * @return Single antique.
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Antique getAntique(@PathParam("id") int id) {
        Optional<Antique> antique = Optional.ofNullable(em.find(Antique.class, id));
        if (antique.isPresent()) {
            return antique.get();
        } else {
            return new Antique();
        }
    }

    /**
     * Method getting single country from db.
     * @param id Searched country's id.
     * @return Single country.
     */
    public Country getCountry(int id) {
        return em.find(Country.class, id);
    }

    /**
     * Method getting all countries from db.
     * @return List of countries.
     */
    public List<Country> getCountries(){
        return em.createQuery("SELECT c FROM Country c").getResultList();
    }

    /**
     * Method updating antique in db.
     * @param antique Antique to be updated.
     */
    public void updateAntique(Antique antique){
        em.merge(antique);
    }

    /**
     * Method adding antique to db.
     * @param antique Antique to be added.
     */
    @Path("")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void createNewAntique(Antique antique) {
        em.persist(antique);
    }

    /**
     * Method adding new order to db.
     * @param order Order to be added.
     */
    public void createNewOrder(Order order) {
        em.persist(order);
    }

    /**
     * Method adding new client to db.
     * @param client Client to be added.
     */
    public void createNewClient(Client client) {
        em.persist(client);
    }

    /**
     * Method finding client by its full name in db.
     * @param name
     * @param surname
     * @return Returns client if exist.
     */
    public Optional<Client> findClientByFullName (String name, String surname) {
        TypedQuery<Client> query = em.createQuery("select a from Client a where a.name = :name and a.surname = :surname", Client.class);
        query.setParameter("name", name);
        query.setParameter("surname", surname);
        try {
            return Optional.of(query.getSingleResult());
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }

}
