package pl.codementors.antiques.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configuration of REST api path.
 */
@ApplicationPath("api")
public class ApplicationConfig extends Application {
}
