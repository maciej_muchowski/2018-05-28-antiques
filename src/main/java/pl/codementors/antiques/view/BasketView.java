package pl.codementors.antiques.view;

import pl.codementors.antiques.AntiquesStore;
import pl.codementors.antiques.model.Antique;
import pl.codementors.antiques.model.Client;
import pl.codementors.antiques.model.Order;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Application scoped basket view.
 */
@Named
@ApplicationScoped
public class BasketView implements Serializable {

    /**
     * EJB store to communicate with db.
     */
    @EJB
    private AntiquesStore store;

    /**
     * List of antiques added to basket.
     */
    private List<Antique> antiquesInBasket = new ArrayList<>();

    /**
     * Placeholder for order.
     */
    private Order order;

    /**
     * Potential client.
     */
    private Client client = new Client();


    public List<Antique> getAntiquesInBasket() {
        return antiquesInBasket;
    }

    public void setAntiquesInBasket(List<Antique> antiquesInBasket) {
        this.antiquesInBasket = antiquesInBasket;
    }

    /**
     * Method adding antique to basket.
     *
     * @param antique Antique to be added.
     */
    public void addAntiqueToBasket(Antique antique) {
        if (!antiquesInBasket.contains(antique)) {
            antiquesInBasket.add(antique);
        }
    }

    /**
     * Method removing antique from basket.
     *
     * @param antique Antique to be removed.
     */
    public void removeAntiqueFromBasket(Antique antique) {
        antiquesInBasket.remove(antique);
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * Method finding client or creating new client and new order for every antique in basket.
     */
    public void makeOrder() {
        Optional<Client> clientByFullName = store.findClientByFullName(client.getName(), client.getSurname());
        if (clientByFullName.isPresent()) {
            client = clientByFullName.get();
        } else {
            store.createNewClient(client);
        }
        for (Antique antique : antiquesInBasket) {
            order = new Order();
            order.setClient(client);
            order.setAntique(antique);
            antique.setAvailable(false);
            store.updateAntique(antique);
            store.createNewOrder(order);
        }
    }
}
