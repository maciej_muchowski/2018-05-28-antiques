package pl.codementors.antiques.view;

import pl.codementors.antiques.AntiquesStore;
import pl.codementors.antiques.model.Antique;

import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Antique edit view.
 */
@ViewScoped
@Named
public class AntiqueEdit implements Serializable {

    /**
     * EJB store to communicate with db.
     */
    @EJB
    private AntiquesStore store;

    /**
     * Antique placeholder.
     */
    private Antique antique;

    /**
     * Antique's id placeholder.
     */
    private int antiqueId;

    /**
     * Placeholder for list of all countries.
     */
    private List<SelectItem> countries;

    /**
     * Filling antique placeholder with antique from db.
     * @return Antique specified by id.
     */
    public Antique getAntique(){
        if (antique == null) {
            antique = store.getAntique(antiqueId);
        }
        return antique;
    }

    public int getAntiqueId() {
        return antiqueId;
    }

    public void setAntiqueId(int antiqueId) {
        this.antiqueId = antiqueId;
    }

    /**
     * Filling countries placeholder with countries from db.
     * @return
     */
    public List<SelectItem> getCountries() {
        if (countries == null) {
            countries = new ArrayList<SelectItem>();
            store.getCountries().forEach(country -> {
               countries.add(new SelectItem(country,country.getName()));
            });
        }
        return countries;
    }

    /**
     * Method saving or updating antique.
     * Depends on id from clicked button. Add button always returns '0' so new antique will be created.
     */
    public void saveAntique(){
        if(antique.getId() == 0){
            store.createNewAntique(antique);
        } else {
            store.updateAntique(antique);
        }
    }
}
