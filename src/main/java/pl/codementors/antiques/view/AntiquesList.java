package pl.codementors.antiques.view;

import pl.codementors.antiques.AntiquesStore;
import pl.codementors.antiques.model.Antique;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Antiques list view.
 */
@ViewScoped
@Named
public class AntiquesList implements Serializable {

    /**
     * EJB store to communicate with db.
     */
    @EJB
    private AntiquesStore store;

    /**
     * Inject of basket.
     */
    @Inject
    private BasketView basketView;

    /**
     * Placeholder for list of all antiques.
     */
    private List<Antique> antiques;

    /**
     * Filling placeholder with antiques from db.
     * @return List of antiques.
     */
    public List<Antique> getAntiques(){
        if(antiques == null){
            antiques = store.findAntiques();
        }
        return antiques;
    }

    /**
     * Adding an antique to basket.
     * @param antique Antique to be added.
     */
    public void addAntiqueToBasket(Antique antique) {
        if (!basketView.getAntiquesInBasket().contains(antique)) {
            basketView.addAntiqueToBasket(antique);
        }
    }

    /**
     * List of antiques added to basket.
     */
    public List<Antique> getAntiquesAddedToBasket() {
        return basketView.getAntiquesInBasket();
    }

    /**
     * Flag that checks if antique is added to basket or is available.
     * @param antique Antique to be checked.
     * @return True if is available and not in basket. Else returns false.
     */
    public boolean addedToBasket (Antique antique) {
        return (basketView.getAntiquesInBasket().contains(antique) || !antique.isAvailable());
    }
}
