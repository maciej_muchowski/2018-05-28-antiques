package pl.codementors.antiques.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Class describing client.
 */
@Entity
@Table(name = "clients")
public class Client implements Serializable {

    /**
     * Client's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Client's name.
     */
    @Column
    private String name;

    /**
     * Client's surname.
     */
    @Column
    private String surname;

    public Client() {
    }

    public Client(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
