package pl.codementors.antiques.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Class describing antique.
 */
@Entity
@Table(name = "antiques")
public class Antique implements Serializable {

    /**
     * Antique's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * Country of origin of antique.
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "country", referencedColumnName = "id")
    private Country country;

    /**
     * Year of making the antique.
     */
    @Column
    private int year;

    /**
     * Price of antique.
     */
    @Column
    private int price;

    /**
     * Boolean to set antique available or not.
     */
    @Column
    private boolean isAvailable;

    public Antique() {
    }

    public Antique(Country country, int year, int price) {
        this.country = country;
        this.year = year;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }
}
