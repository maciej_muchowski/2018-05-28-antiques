package pl.codementors.antiques.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Class describing order.
 */
@Entity
@Table(name = "orders")
public class Order implements Serializable {

    /**
     * Order's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Antique ordered.
     */
    @OneToOne
    @JoinColumn(name = "antique", referencedColumnName = "id")
    private Antique antique;

    /**
     * Client who made an order.
     */
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "client", referencedColumnName = "id")
    private Client client;

    public Order() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Antique getAntique() {
        return antique;
    }

    public void setAntique(Antique antique) {
        this.antique = antique;
    }
}
